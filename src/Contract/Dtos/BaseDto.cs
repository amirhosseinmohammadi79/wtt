﻿namespace Contract.Dtos;

public class BaseDto
{
    public string? Username { get; set; }
    public string? WorkSpace { get; set; }
}