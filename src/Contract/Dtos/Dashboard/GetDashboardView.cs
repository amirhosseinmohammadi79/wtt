﻿namespace Contract.Dtos.Dashboard;

public class GetDashboardView : BaseDto
{
    public DateTime? FromDate { get; set; }
    public DateTime? ToDate { get; set; }
}