﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Contract.Dtos.Vacation
{
    public class UpdateVacationDto 
    {
        public string Id { get; set; }
        public DateTime Date { get; set; }
        public string Description { get; set; }
    }
}
