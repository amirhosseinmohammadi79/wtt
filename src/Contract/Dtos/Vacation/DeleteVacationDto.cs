﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Contract.Dtos.Vacation
{
    public class DeleteVacationDto
    {
        public string Id { get; set; }
    }
}
