﻿namespace Contract.Dtos.User;

public class DeleteUserDto
{
    public string Username { get; set; }
    public string WorkSpace { get; set; }
}