﻿namespace Contract.Dtos.User;

public class LoginResultDto
{
    public string Token { get; set; }
    public string Message { get; set; }
}