﻿namespace Contract.Dtos.User;

public class UpdateUserDto
{
    public string Username { get; set; }
    public string FirstName { get; set; }
    public string LastName { get; set; }
    public string WorkSpace { get; set; }
    public string Role { get; set; }
    public string NationalId { get; set; }
    public string PhoneNumber { get; set; }
    public string Email { get; set; }
}