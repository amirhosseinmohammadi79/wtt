﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Contract.Dtos.Duty
{
    public class RegisterDutyDto : BaseDto
    {
        public string Title { get; set; }
        public string ProjectName { get; set; }
        public string WorkPlace { get; set; }
        public DateTime Date { get; set; }
        public int Time { get; set; }
        public string Description { get; set; }
    }
}
