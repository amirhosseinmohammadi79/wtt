﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Contract.Dtos.Duty
{
    public class DeleteDutyDto : BaseDto
    {
        public string Id { get; set; }
    }
}
