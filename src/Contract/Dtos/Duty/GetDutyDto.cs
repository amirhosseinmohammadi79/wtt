﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Contract.Dtos.Duty
{
    public class GetDutyDto : BaseDto
    {
        public string? Title { get; set; } = string.Empty;
        public string? ProjectName { get; set; } = string.Empty;
        public DateTime? FromDate { get; set; } 
        public DateTime? ToDate { get; set; } 
    }
}
