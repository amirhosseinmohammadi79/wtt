﻿namespace Core.Settings;

public class Settings : ISettings
{
    public static Settings AllSettings { get; private set; }
    public ElasticSettings ElasticSettings { get; set; }
    public JwtSettings JwtSettings { get; set; }

    public static void Set(Settings settings)
    {
        AllSettings = settings;
    }
}