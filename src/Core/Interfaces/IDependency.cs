﻿namespace Core.Interfaces;

// Just to mark
public interface IScopedDependency
{
}

public interface ITransientDependency
{
}

public interface ISingletonDependency
{
}