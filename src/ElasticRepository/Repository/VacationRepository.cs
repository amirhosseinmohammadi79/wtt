﻿using Contract.Dtos.Presence;
using Contract.Dtos.Vacation;
using Core.Settings;
using Elasticsearch.Net;
using Entities.Models;
using Nest;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ElasticRepository.Repository
{
    public class VacationRepository
    {
        private readonly IElasticClient? _elasticClient;

        public VacationRepository()
        {
            _elasticClient = new ElasticClient(
                new ConnectionSettings(new Uri(Settings.AllSettings.ElasticSettings.Host))
                    .BasicAuthentication(Settings.AllSettings.ElasticSettings.Username,
                        Settings.AllSettings.ElasticSettings.Password));
        }


        public async Task RegisterVacation(Vacation vacation, string username, string workSpace)
        {
            var index = $"{username}_{workSpace}_vacations";
            var indexResponse = await _elasticClient?.IndexAsync(vacation, i => i
                .Index(index)
                .Id(vacation.Id));

            if (indexResponse.Result != Result.Created)
                throw new Exception("vacation couldn't be added to elastic");
        }

        public async Task UpdateVacation(Vacation vacation, string username, string workSpace)
        {
            var currentVacation = await GetVacationById(username, workSpace, vacation.Id);
            if (currentVacation == null)
                throw new Exception($"Id : {vacation.Id} not Exist !!");

            if (currentVacation.Accepted)
                throw new Exception($"This Vacation is Accepted !! you can not update this !");

            var indexName = $"{username}_{workSpace}_vacations";
            var updateResponse = await _elasticClient.UpdateAsync<Vacation, object>(vacation.Id, u => u
                .Index(indexName)
                .Doc(new Vacation
                {
                    Date = vacation.Date,
                    Description = vacation.Description,
                    RegDateTime = DateTime.Now
                })
                .Refresh(Refresh.True));
        }


        public async Task<Vacation> GetVacationById(string username, string workSpace, string id)
        {
            var indexName = $"{username}_{workSpace}_vacations";
            var result = await _elasticClient.SearchAsync<Vacation>(s => s
                .Index(indexName)
                .Query(q => q
                    .Match(m => m
                        .Field(f => f.Id)
                        .Query(id))));
            return result.Documents.FirstOrDefault();
        }


        public async Task<List<Vacation>> GetVacations(GetVacationDto getVacationDto)
        {
            var index = $"{getVacationDto.Username}_{getVacationDto.WorkSpace}_vacations";
            var fromDate = getVacationDto.FromDate ?? new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
            var toDate = getVacationDto.ToDate ?? new DateTime(DateTime.Now.Year, DateTime.Now.Month,
                DateTime.DaysInMonth(DateTime.Now.Year, DateTime.Now.Month));

            var response = await _elasticClient.SearchAsync<Vacation>(i => i
                .Index(index)
                .Query(q => q
                    .Bool(b => b
                        .Filter(f => f
                                .Term(t => t.IsDeleted, false) && f
                                .DateRange(dr => dr
                                    .Field(p => p.Date)
                                    .GreaterThanOrEquals(fromDate)
                                    .LessThanOrEquals(toDate))
                        )))
                .Sort(sort => sort.Descending(p => p.Date))
            );

            return response.Documents.ToList();
        }


        public async Task DeleteVacation(string username, string workSpace, string id)
        {
            var vacation = await GetVacationById(username, workSpace, id);
            if (vacation != null)
                throw new Exception($"Id : {id} not Exist !!");

            var indexName = $"{username}_{workSpace}_vacations";
            var response = await _elasticClient.UpdateAsync<Vacation, object>(id, u => u.Index(indexName)
                .Doc(new Vacation { IsDeleted = true }));
        }
    }
}