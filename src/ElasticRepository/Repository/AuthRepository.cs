﻿using Core.Settings;
using ElasticRepository.Services;
using Entities.Models;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using Nest;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;


namespace ElasticRepository.Repository
{
    public class AuthRepository
    {
        private readonly IElasticClient? _elasticClient;
        private readonly IConfiguration _configuration;

        public AuthRepository(IConfiguration configuration)
        {
            _configuration = configuration;
            _elasticClient = new ElasticClient(
             new ConnectionSettings(new Uri(Settings.AllSettings.ElasticSettings.Host))
                 .BasicAuthentication(Settings.AllSettings.ElasticSettings.Username,
                     Settings.AllSettings.ElasticSettings.Password));
        }

        public async Task<User> GetUser(string username)
        {
            var indexName = $"*_users";
            var result = await _elasticClient.SearchAsync<User>(s => s
                .Index(indexName)
                .Query(q => q
                    .Bool(b => b
                        .Must(
                            m => m.Match(mt => mt.Field(f => f.Username).Query(username)),
                            m => m.Term(t => t.Field(f => f.IsDeleted).Value(false))
                        )
                    )
                )
            );
            return result.Documents.FirstOrDefault();

        }

        public async Task<string> Authenticate(string username, string password)
        {
            var user = await GetUser(username);
            if (user == null)
                throw new Exception("نام کاربری یا کلمه عبور نامعتبر است!");

            var encryptedPassword = EncryptionService.Encrypt(password);
            if (user.Username == username && user.Password == encryptedPassword)
            {
              return await GenerateToken(user);             
            }
            else
            {
                throw new Exception("نام کاربری یا کلمه عبور نامعتبر است!");
            }
        }


        public Task<string> GenerateToken(User user)
        {
            var securityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(Settings.AllSettings.JwtSettings.SecretKey));
            var credentials = new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256);
            var claims = new[]
            {
                new Claim("Username",user.Username),
                new Claim("Role",user.Role),
                new Claim("WorkSpace",user.WorkSpace),
            };
            var token = new JwtSecurityToken(Settings.AllSettings.JwtSettings.Issuer,
               Settings.AllSettings.JwtSettings.Audience,
                claims,
                expires: DateTime.Now.AddDays(1),
                signingCredentials: credentials);

         return Task.FromResult(new JwtSecurityTokenHandler().WriteToken(token));
        }
    }
}
