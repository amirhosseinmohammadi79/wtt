﻿using Contract.Dtos.Presence;
using Contract.Dtos.User;
using Core.Settings;
using Elasticsearch.Net;
using Entities.Models;
using Nest;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ElasticRepository.Repository
{
    public class UserRepository
    {
        private readonly IElasticClient? _elasticClient;

        public UserRepository()
        {
            _elasticClient = new ElasticClient(
                new ConnectionSettings(new Uri(Settings.AllSettings.ElasticSettings.Host))
                    .BasicAuthentication(Settings.AllSettings.ElasticSettings.Username,
                        Settings.AllSettings.ElasticSettings.Password));
        }

        public async Task RegisterUser(User user)
        {
            CheckUsernameExistence(user.Username , user.WorkSpace);

            var index = $"{user.WorkSpace}_users";
            var indexResponse = await _elasticClient?.IndexAsync(user, i => i
                .Index(index)
                .Id(user.Username));

            if (indexResponse.Result != Result.Created)
                throw new Exception("User couldn't be added to elastic");
        }

        public async Task UpdateUser(User user , string workSpace)
        {
            var currentUser = await GetUser(user.Username , workSpace );
            if (currentUser == null)
                throw new Exception($"Username : {user.Username} not Fount !!");
            
            var index = $"{workSpace}_users";
            var updateResponse = await _elasticClient.UpdateAsync<User, object>(user.Username, u => u
                .Index(index)
                .Doc(new User
                {
                    Email = user.Email,
                    Password = user.Password,
                    FirstName = user.FirstName,
                    LastName = user.LastName,
                    Role = user.Role,
                    PhoneNumber = user.PhoneNumber,
                    NationalId = user.NationalId,
                    RegDateTime = user.RegDateTime
                })
                .Refresh(Refresh.True));
        }
        
        public async Task<User> GetUser(string username ,string workSpace)
        {
            var indexName = $"{workSpace}_users";
            var result = await _elasticClient.SearchAsync<User>(s => s
                .Index(indexName)
                .Query(q => q
                    .Match(m => m
                        .Field(f => f.Username)
                        .Query(username))));
            return result.Documents.FirstOrDefault();
        }


        public async Task CheckUsernameExistence(string username, string workSpace)
        {
            var indexName = $"{workSpace}_users";
            var result = await _elasticClient.SearchAsync<User>(s => s
                .Index(indexName)
                .Query(q => q
                    .Match(m => m
                        .Field(f => f.Username)
                        .Query(username))));

            var isExist = result.Documents.Any();
            if (isExist)
                throw new Exception($"User With THis Username : {username} is exist !!");
        }

        public async Task<List<UserResponse>> GetUsers(GetUsersDto getUsersDto)
        {
            var index = "*_users";
            var response = await _elasticClient.SearchAsync<UserResponse>(i => i
                    .Index(index)
                    .Query(q => q
                        .Bool(b => b
                            .Filter(f => f
                                    .Term(t => t.IsDeleted, false) && f
                                    .Wildcard(w => w.Field(u => u.Username).Value($"*{getUsersDto.Username}*")) && f
                                    .Wildcard(w => w.Field(n => n.FirstName).Value($"*{getUsersDto.FirstName}*")) && f
                                    .Wildcard(w => w.Field(l => l.LastName).Value($"*{getUsersDto.LastName}*")) && f
                                    .Wildcard(w => w.Field(i => i.NationalId).Value($"*{getUsersDto.NationalId}*"))
                            )))
                // .Sort(sort => sort.Ascending(p => p.Username))
            );

            return response.Documents.ToList();
        }


        public async Task DeleteUser(string username, string workSpace)
        {
            var user = await GetUser(username , workSpace);
            if (user == null)
                throw new Exception($"Username : {username} not Fount !!");

            var indexName = $"{workSpace}_users";
            var response = await _elasticClient.UpdateAsync<User, object>(username, u => u.Index(indexName)
                .Doc(new User { IsDeleted = true }));
        }
    }
}