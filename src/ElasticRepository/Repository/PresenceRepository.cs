﻿using Contract.Dtos.Duty;
using Contract.Dtos.Presence;
using Core.Settings;
using Elasticsearch.Net;
using Entities.Models;
using Nest;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ElasticRepository.Repository
{
    public class PresenceRepository
    {
        private readonly IElasticClient? _elasticClient;

        public PresenceRepository()
        {
            _elasticClient = new ElasticClient(
                new ConnectionSettings(new Uri(Settings.AllSettings.ElasticSettings.Host))
                    .BasicAuthentication(Settings.AllSettings.ElasticSettings.Username,
                        Settings.AllSettings.ElasticSettings.Password));
        }

        public async Task RegisterPresence(Presence presence, string username, string workSpace)
        {
            var index = $"{username}_{workSpace}_presences";
            var indexResponse = await _elasticClient?.IndexAsync(presence, i => i
                .Index(index)
                .Id(presence.Id));

            if (indexResponse.Result != Result.Created)
                throw new Exception("Presence couldn't be added to elastic");
        }

        public async Task UpdatePresence(Presence presence, string username, string workSpace)
        {
            var currentPresence = await GetPresenceById(username, workSpace, presence.Id);
            if (currentPresence == null)
                throw new Exception($"Id : {presence.Id} not Exist !!");

            var indexName = $"{username}_{workSpace}_presences";
            var updateResponse = await _elasticClient.UpdateAsync<Presence, object>(presence.Id, u => u
                .Index(indexName)
                .Doc(new Presence
                {
                    Date = presence.Date,
                    StartTime = presence.StartTime,
                    EndTime = presence.EndTime,
                    TotalPresenceTime = presence.TotalPresenceTime,
                })
                .Refresh(Refresh.True));
        }


        public async Task<Presence> GetPresenceById(string username, string workSpace, string id)
        {
            var indexName = $"{username}_{workSpace}_presences";
            var result = await _elasticClient.SearchAsync<Presence>(s => s
                .Index(indexName)
                .Query(q => q
                    .Match(m => m
                        .Field(f => f.Id)
                        .Query(id))));
            return result.Documents.FirstOrDefault();
        }


        public async Task<List<Presence>> GetPresences(GetPresenceDto getPresenceDto)
        {
            var index = $"{getPresenceDto.Username}_{getPresenceDto.WorkSpace}_presences";
            var fromDate = getPresenceDto.FromDate ?? new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
            var toDate = getPresenceDto.ToDate ?? new DateTime(DateTime.Now.Year, DateTime.Now.Month,
                DateTime.DaysInMonth(DateTime.Now.Year, DateTime.Now.Month));

            var response = await _elasticClient.SearchAsync<Presence>(i => i
                .Index(index)
                .Query(q => q
                    .Bool(b => b
                        .Filter(f => f
                                .Term(t => t.IsDeleted, false) && f
                                .Term(t => t.Id, getPresenceDto.Id) && f
                                .DateRange(dr => dr
                                    .Field(p => p.Date)
                                    .GreaterThanOrEquals(fromDate)
                                    .LessThanOrEquals(toDate))
                        )))
                .Sort(sort => sort.Descending(p => p.Date))
            );

            return response.Documents.ToList();
        }


        public async Task DeletePreence(string username, string workSpace, string id)
        {
            var presence = await GetPresenceById(username, workSpace, id);
            if (presence == null)
                throw new Exception($"Id : {id} not Exist !!");

            var indexName = $"{username}_{workSpace}_presences";
            var response = await _elasticClient.UpdateAsync<Presence, object>(id, u => u.Index(indexName)
                .Doc(new Presence { IsDeleted = true }));
        }
    }
}