﻿using Contract.Dtos.Duty;
using Core.Settings;
using Elasticsearch.Net;
using Entities.Models;
using Nest;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace ElasticRepository.Repository
{
    public class DutyRepository
    {
        private readonly IElasticClient? _elasticClient;

        public DutyRepository()
        {
            _elasticClient = new ElasticClient(
             new ConnectionSettings(new Uri(Settings.AllSettings.ElasticSettings.Host))
                 .BasicAuthentication(Settings.AllSettings.ElasticSettings.Username,
                     Settings.AllSettings.ElasticSettings.Password));
        }


        public async Task RegisterDuty(Duty duty, string username, string workSpace)
        {
            var index = $"{username}_{workSpace}_duty";

            var indexResponse = await _elasticClient?.IndexAsync(duty, i => i
                .Index(index)
                .Id(duty.Id));

            if (indexResponse.Result != Result.Created)
                throw new Exception("Duty couldn't be added to elastic");

        }

        public async Task UpdateDuty(Duty duty, string username, string workSpace)
        {
            var indexName = $"{username}_{workSpace}_duty";
            var updateResponse = await _elasticClient.UpdateAsync<Duty, object>(duty.Id, u => u
              .Index(indexName)
              .Doc(new Duty
              {
                  Date = duty.Date,
                  Description = duty.Description,
                  ProjectName = duty.ProjectName,
                  Time = duty.Time,
                  Title = duty.Title,
                  WorkPlace = duty.WorkPlace,
                  RegDateTime = duty.RegDateTime
              })
              .Refresh(Refresh.True));
        }

        public async Task DeleteDuty(string username, string workSpace, string id)
        {
            var duty = await GetDutyById(username, workSpace, id);
            if (duty == null)
                throw new Exception($"Id : {id} not Exist !!");

            var indexName = $"{username}_{workSpace}_duty";
            var response = await _elasticClient.UpdateAsync<Duty, object>(id, u => u.Index(indexName)
              .Doc(new Duty { IsDeleted = true }));
        }

        public async Task<Duty> GetDutyById(string username, string workSpace, string id)
        {
            var indexName = $"{username}_{workSpace}_duty";
            var result = await _elasticClient.SearchAsync<Duty>(s => s
                .Index(indexName)
                .Query(q => q
                    .Match(m => m
                        .Field(f => f.Id)
                        .Query(id))));
            return result.Documents.FirstOrDefault();

        }

        public async Task<List<Duty>> GetDuties(GetDutyDto dutyDto)
        {

            var index = $"{dutyDto.Username}_{dutyDto.WorkSpace}_duty";
            var fromDate = dutyDto.FromDate ?? new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
            var toDate = dutyDto.ToDate ?? new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.DaysInMonth(DateTime.Now.Year, DateTime.Now.Month));
            var response = await _elasticClient.SearchAsync<Duty>(i => i
              .Index(index)
              .Query(q => q
                .Bool(b => b
                  .Filter(f => f
                    .Term(t => t.Title, dutyDto.Title) && f
                      .Term(t => t.ProjectName, dutyDto.ProjectName) && f
                        .Term(t => t.IsDeleted, false) && f
                        .DateRange(dr => dr
                        .Field(p => p.Date)
                        .GreaterThanOrEquals(fromDate)
                        .LessThanOrEquals(toDate))
                  )))
              .Sort(sort => sort.Descending(p => p.RegDateTime))
            );

            return response.Documents.ToList();
        }

    }
}
