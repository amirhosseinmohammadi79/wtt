﻿using System.Security.Cryptography;
using System.Text;

namespace ElasticRepository.Services;

public class EncryptionService
{
    public static string Encrypt(string input)
    {
        using (var md5 = MD5.Create())
        {
            byte[] inputBytes = Encoding.UTF8.GetBytes(input);
            byte[] hashBytes = md5.ComputeHash(inputBytes);
            return Convert.ToBase64String(hashBytes);
        }
    }
}