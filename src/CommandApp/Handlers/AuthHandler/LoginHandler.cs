﻿using CommandApp.Commands.AuthCommands;
using Contract.Dtos.User;
using ElasticRepository.Repository;
using MediatR;

namespace CommandApp.Handlers.AuthHandler
{
    public class LoginHandler : IRequestHandler<LoginCommand, LoginResultDto>
    {
        private readonly AuthRepository _authRepository;

        public LoginHandler(AuthRepository authRepository)
        {
            _authRepository = authRepository;
        }

        public async Task<LoginResultDto> Handle(LoginCommand request, CancellationToken cancellationToken)
        {
            var token = await _authRepository.Authenticate(request.Username, request.Password);
            var result = new LoginResultDto()
            {
                Token = token,
                Message = "با موفقیت وارد شدید"
            };
            return result;
        }
    }
}