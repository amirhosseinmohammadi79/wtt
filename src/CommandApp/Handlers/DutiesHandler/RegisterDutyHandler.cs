﻿using CommandApp.Commands.Duty;
using ElasticRepository.Repository;
using Entities.Models;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommandApp.Handlers.DutiesHandler
{
    public class RegisterDutyHandler : IRequestHandler<RegisterDutyCommand>
    {
        private readonly DutyRepository _dutyRepository;
        public RegisterDutyHandler(DutyRepository dutyRepository)
        {
            _dutyRepository = dutyRepository;
        }
        public async Task<Unit> Handle(RegisterDutyCommand request, CancellationToken cancellationToken)
        {
            var duty = new Duty
            {
                Id = Guid.NewGuid().ToString().Replace("-", ""),
                Date = request.Date,
                Description = request.Description,
                ProjectName = request.ProjectName,
                Time = request.Time,
                Title = request.Title,
                WorkPlace = request.WorkPlace,
                RegDateTime = DateTime.Now,
                IsDeleted = false
            };
            await _dutyRepository.RegisterDuty(duty, request.Username, request.WorkSpace);
            return Unit.Value;
        }
    }
}
