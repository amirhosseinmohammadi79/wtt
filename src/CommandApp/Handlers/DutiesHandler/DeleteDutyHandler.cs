﻿using CommandApp.Commands.DutiesCommands;
using CommandApp.Commands.Duty;
using ElasticRepository.Repository;
using Entities.Models;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommandApp.Handlers.DutiesHandler
{
    public class DeleteDutyHandler : IRequestHandler<DeleteDutyCommand>
    {
        private readonly DutyRepository _dutyRepository;
        public DeleteDutyHandler(DutyRepository dutyRepository)
        {
            _dutyRepository = dutyRepository;
        }
        public async Task<Unit> Handle(DeleteDutyCommand request, CancellationToken cancellationToken)
        {
            await _dutyRepository.DeleteDuty( request.Username, request.WorkSpace , request.Id);
            return Unit.Value;
        }
    }
}
