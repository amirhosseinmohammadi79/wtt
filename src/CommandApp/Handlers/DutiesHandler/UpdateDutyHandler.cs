﻿using CommandApp.Commands.DutiesCommands;
using CommandApp.Commands.Duty;
using ElasticRepository.Repository;
using Entities.Models;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommandApp.Handlers.DutiesHandler
{
    public class UpdateDutyHandler : IRequestHandler<UpdateDutyCommand>
    {
        private readonly DutyRepository _dutyRepository;
        public UpdateDutyHandler(DutyRepository dutyRepository)
        {
            _dutyRepository = dutyRepository;
        }
        public async Task<Unit> Handle(UpdateDutyCommand request, CancellationToken cancellationToken)
        {
            var duty = new Duty
            {
                Id = request.Id,
                Date = request.Date,
                Description = request.Description,
                ProjectName = request.ProjectName,
                Time = request.Time,
                Title = request.Title,
                WorkPlace = request.WorkPlace,
                RegDateTime = DateTime.Now,
                IsDeleted = false
            };
            await _dutyRepository.UpdateDuty(duty, request.Username, request.WorkSpace);
            return Unit.Value;
        }
    }
}
