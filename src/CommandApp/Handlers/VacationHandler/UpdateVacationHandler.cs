﻿using CommandApp.Commands.PresencesCommand;
using CommandApp.Commands.VacationCommand;
using ElasticRepository.Repository;
using Entities.Models;
using MediatR;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommandApp.Handlers.VacationHandler
{
    public class UpdateVacationHandler:IRequestHandler<UpdateVacationCommand>
    {
        private readonly VacationRepository _vacationRepository;
        public UpdateVacationHandler(VacationRepository vacationRepository)
        {
            _vacationRepository = vacationRepository;
        }

        public async Task<Unit> Handle(UpdateVacationCommand request, CancellationToken cancellationToken)
        {
            var vacation = new Vacation
            {
                Id = request.Id,
                Date = request.Date,
                RegDateTime = DateTime.Now,
                Description = request.Description,
            };

            await _vacationRepository.UpdateVacation(vacation, request.Username, request.WorkSpace);
            return Unit.Value;
        }
    }
}
