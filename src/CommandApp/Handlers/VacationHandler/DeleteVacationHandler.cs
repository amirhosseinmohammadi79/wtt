﻿using CommandApp.Commands.PresencesCommand;
using CommandApp.Commands.VacationCommand;
using ElasticRepository.Repository;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommandApp.Handlers.VacationHandler
{
    public class DeleteVacationHandler : IRequestHandler<DeleteVacationCommand>
    {
        private readonly VacationRepository _vacationRepository;
        public DeleteVacationHandler(VacationRepository vacationRepository)
        {
            _vacationRepository = vacationRepository;
        }
        public async Task<Unit> Handle(DeleteVacationCommand request, CancellationToken cancellationToken)
        {
            await _vacationRepository.DeleteVacation(request.Username, request.WorkSpace, request.Id);
            return Unit.Value;
        }
    }
}
