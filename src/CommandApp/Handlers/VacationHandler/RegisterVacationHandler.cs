﻿using CommandApp.Commands.PresencesCommand;
using CommandApp.Commands.VacationCommand;
using ElasticRepository.Repository;
using Entities.Models;
using MediatR;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommandApp.Handlers.VacationHandler
{
    public class RegisterVacationHandler : IRequestHandler<RegisterVacationCommand>
    {
        private readonly VacationRepository _vacationRepository;
        public RegisterVacationHandler(VacationRepository vacationRepository)
        {
            _vacationRepository = vacationRepository;
        }

        public async Task<Unit> Handle(RegisterVacationCommand request, CancellationToken cancellationToken)
        {
            var vacation = new Vacation
            {
                Id = Guid.NewGuid().ToString().Replace("-", ""),
                Date = request.Date,
                IsDeleted = false,
                Accepted = false,
                Description = request.Description,
                RegDateTime = DateTime.Now,
            };

            await _vacationRepository.RegisterVacation(vacation, request.Username, request.WorkSpace);
            return Unit.Value;

        }

    }
}
