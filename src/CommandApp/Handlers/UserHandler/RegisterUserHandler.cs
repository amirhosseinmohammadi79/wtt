﻿using CommandApp.Commands.Duty;
using CommandApp.Commands.UserCommand;
using ElasticRepository.Repository;
using ElasticRepository.Services;
using Entities.Models;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommandApp.Handlers.UserHandler
{
    public class RegisterUserHandler : IRequestHandler<RegisterUserCommand>
    {
        private readonly UserRepository _userRepository;
        public RegisterUserHandler(UserRepository userRepository)
        {
            _userRepository = userRepository;
        }

        public async Task<Unit> Handle(RegisterUserCommand request, CancellationToken cancellationToken)
        {
            var user = new User
            {
                Username = request.Username,
                Email = request.Email,
                PhoneNumber = request.PhoneNumber,
                FirstName = request.FirstName,
                LastName = request.LastName,
                NationalId = request.NationalId,
                WorkSpace = request.WorkSpace,
                Password = EncryptionService.Encrypt(request.Password),
                RegDateTime = DateTime.Now,
                Role = request.Role,
                IsDeleted = false
            };
            await _userRepository.RegisterUser(user);
            return Unit.Value;
        }

    }
}
