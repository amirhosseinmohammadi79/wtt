﻿using CommandApp.Commands.UserCommand;
using ElasticRepository.Repository;
using MediatR;

namespace CommandApp.Handlers.UserHandler;

public class DeleteUserHandler : IRequestHandler<DeleteUserCommand>
{
    private readonly UserRepository _userRepository;
    public DeleteUserHandler(UserRepository userRepository)
    {
        _userRepository = userRepository;
    }
    
    public async Task<Unit> Handle(DeleteUserCommand request, CancellationToken cancellationToken)
    {
        await _userRepository.DeleteUser(request.Username, request.WorkSpace);
        return Unit.Value;
    }
}