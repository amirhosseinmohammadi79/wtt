﻿using CommandApp.Commands.UserCommand;
using ElasticRepository.Repository;
using ElasticRepository.Services;
using Entities.Models;
using MediatR;

namespace CommandApp.Handlers.UserHandler;

public class UpdateUserHandler : IRequestHandler<UpdateUserCommand>
{
    private readonly UserRepository _userRepository;
    
    public UpdateUserHandler(UserRepository userRepository)
    {
        _userRepository = userRepository;
    }

    public async Task<Unit> Handle(UpdateUserCommand request, CancellationToken cancellationToken)
    {
        var user = new User
        {
            Username = request.Username,
            Email = request.Email,
            PhoneNumber = request.PhoneNumber,
            FirstName = request.FirstName,
            LastName = request.LastName,
            NationalId = request.NationalId,
            RegDateTime = DateTime.Now,
            Role = request.Role,
        };

        await _userRepository.UpdateUser(user , request.WorkSpace);
        return Unit.Value;
    }
}