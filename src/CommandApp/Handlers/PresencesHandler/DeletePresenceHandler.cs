﻿using CommandApp.Commands.DutiesCommands;
using CommandApp.Commands.PresencesCommand;
using ElasticRepository.Repository;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommandApp.Handlers.PresencesHandler
{
    public class DeletePresenceHandler : IRequestHandler<DeletePresenceCommand>
    {
        private readonly PresenceRepository _presenceRepository;
        public DeletePresenceHandler(PresenceRepository presenceRepository)
        {
            _presenceRepository = presenceRepository;
        }

        public async Task<Unit> Handle(DeletePresenceCommand request, CancellationToken cancellationToken)
        {
            await _presenceRepository.DeletePreence(request.Username, request.WorkSpace, request.Id);
            return Unit.Value;
        }

    }
}
