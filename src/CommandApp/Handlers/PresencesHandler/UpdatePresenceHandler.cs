﻿using CommandApp.Commands.PresencesCommand;
using ElasticRepository.Repository;
using Entities.Models;
using MediatR;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommandApp.Handlers.PresencesHandler
{
    public class UpdatePresenceHandler : IRequestHandler<UpdatePresenceCommand>
    {
        private readonly PresenceRepository _presenceRepository;
        public UpdatePresenceHandler(PresenceRepository presenceRepository)
        {
            _presenceRepository = presenceRepository;
        }

        public async Task<Unit> Handle(UpdatePresenceCommand request, CancellationToken cancellationToken)
        {
            DateTime startDate = DateTime.ParseExact(request.StartTime, "HH:mm:ss", CultureInfo.InvariantCulture);
            DateTime endDate = DateTime.ParseExact(request.EndTime, "HH:mm:ss", CultureInfo.InvariantCulture);

            var presence = new Presence
            {
                Id = request.Id,
                Date = request.Date,
                EndTime = request.EndTime,
                StartTime = request.StartTime,
                IsDeleted = false,
                RegDateTime = DateTime.Now,
                TotalPresenceTime = (endDate - startDate).ToString()
            };

            await _presenceRepository.UpdatePresence(presence, request.Username, request.WorkSpace);
            return Unit.Value;
        }
    }
}
