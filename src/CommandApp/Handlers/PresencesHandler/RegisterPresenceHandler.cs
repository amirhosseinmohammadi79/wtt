﻿using CommandApp.Commands.PresencesCommand;
using ElasticRepository.Repository;
using Entities.Models;
using MediatR;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommandApp.Handlers.PresencesHandler
{
    public class RegisterPresenceHandler : IRequestHandler<RegisterPresenceCommand>
    {
        private readonly PresenceRepository _presenceRepository;
        public RegisterPresenceHandler(PresenceRepository presenceRepository)
        {
            _presenceRepository = presenceRepository;
        }

        public async Task<Unit> Handle(RegisterPresenceCommand request, CancellationToken cancellationToken)
        {
            DateTime startDate = DateTime.ParseExact(request.StartTime, "HH:mm:ss", CultureInfo.InvariantCulture);
            DateTime endDate = DateTime.ParseExact(request.EndTime, "HH:mm:ss", CultureInfo.InvariantCulture);

            var presence = new Presence
            {
                Id = Guid.NewGuid().ToString().Replace("-", ""),
                Date = request.Date,
                EndTime = request.EndTime,
                StartTime = request.StartTime,
                IsDeleted = false,
                RegDateTime= DateTime.Now,
                TotalPresenceTime = (endDate - startDate).ToString()
             };

            await _presenceRepository.RegisterPresence(presence , request.Username, request.WorkSpace);
            return Unit.Value;

        }
    }
}
