﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommandApp.Commands.PresencesCommand
{
    public class DeletePresenceCommand : BaseCommand , IRequest
    {
        public DeletePresenceCommand(string username , string workSpace , string id) : base(username , workSpace)
        {
            Id = id;
        }

        public string Id { get; set; }
    }
}
