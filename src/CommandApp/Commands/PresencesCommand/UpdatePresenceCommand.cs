﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommandApp.Commands.PresencesCommand
{
    public class UpdatePresenceCommand : BaseCommand , IRequest
    {
        public UpdatePresenceCommand(string username , string workSpace, string id , DateTime date , string startTime , string endTime) : base(username , workSpace)
        {
            Id = id;
            Date = date;
            StartTime = startTime;
            EndTime = endTime;
        }

        public DateTime Date { get; set; }
        public string StartTime { get; set; }
        public string Id { get; set; }
        public string EndTime { get; set; }
    }
}
