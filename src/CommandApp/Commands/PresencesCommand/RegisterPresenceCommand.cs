﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommandApp.Commands.PresencesCommand
{
    public class RegisterPresenceCommand : BaseCommand, IRequest
    {
        public RegisterPresenceCommand(string username , string workSpace , DateTime date , string startTime , string endTime) : base(username , workSpace)
        {
            Date = date ;
            StartTime = startTime ;
            EndTime = endTime ;
        }

        public DateTime Date { get; set; }
        public string StartTime { get; set; }
        public string EndTime { get; set; }
    }
}
