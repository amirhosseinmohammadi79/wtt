﻿using MediatR;

namespace CommandApp.Commands.UserCommand;

public class DeleteUserCommand : IRequest
{
    public DeleteUserCommand(string username, string workSpace)
    {
        Username = username;
        WorkSpace = workSpace;
    }

    public string Username { get; set; }
    public string WorkSpace { get; set; }
}