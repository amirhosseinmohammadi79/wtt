﻿using MediatR;

namespace CommandApp.Commands.UserCommand;

public class UpdateUserCommand : IRequest
{
    public UpdateUserCommand(string username ,string firstName , string lastName , 
        string role , string nationalId , string phoneNumber , string email , string workSpace )
    {
        Username = username;
        FirstName = firstName;
        LastName = lastName;
        Role = role;
        NationalId = nationalId;
        PhoneNumber = phoneNumber;
        Email = email;
        WorkSpace = workSpace;
    }
    
    public string Username { get; set; }
    public string FirstName { get; set; }
    public string LastName { get; set; }
    public string WorkSpace { get; set; }
    public string Role { get; set; }
    public string NationalId { get; set; }
    public string PhoneNumber { get; set; }
    public string Email { get; set; }
}