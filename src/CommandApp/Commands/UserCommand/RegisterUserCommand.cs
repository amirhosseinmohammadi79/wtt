﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommandApp.Commands.UserCommand
{
    public class RegisterUserCommand : IRequest
    {
        public RegisterUserCommand(string username, string firstName, string lastName, string password, string role, string nationalId, string phoneNumber, string email, string workSpace)
        {
            Username = username;
            FirstName = firstName;
            LastName = lastName;
            Password = password;
            Role = role;
            NationalId = nationalId;
            Email = email;
            PhoneNumber = phoneNumber;
            WorkSpace = workSpace;
        }

        public string Username { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Password { get; set; }
        public string WorkSpace { get; set; }
        public string Role { get; set; }
        public string NationalId { get; set; }
        public string PhoneNumber { get; set; }
        public string Email { get; set; }
    }
}
