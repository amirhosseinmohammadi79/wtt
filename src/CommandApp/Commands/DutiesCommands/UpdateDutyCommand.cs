﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommandApp.Commands.DutiesCommands
{
    public class UpdateDutyCommand : BaseCommand, IRequest
    {
        public UpdateDutyCommand(string username, string workSpace, string id,
            string title, string projectName, string workPlace, DateTime date
            , int time, string description) : base(username , workSpace)
        {
            Id = id;
            Title = title;
            ProjectName = projectName;
            WorkPlace = workPlace;
            Date = date;
            Time = time;
            Description = description;

        }
        public string Id { get; set; }
        public string Title { get; set; }
        public string ProjectName { get; set; }
        public string WorkPlace { get; set; }
        public DateTime Date { get; set; }
        public int Time { get; set; }
        public string Description { get; set; }
    }

}
