﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommandApp.Commands.Duty
{
    public class RegisterDutyCommand  :BaseCommand , IRequest
    {
        public RegisterDutyCommand(string username , string workSpace , string title , string projectName , 
            string workPlace , DateTime date , int time , string description) : base(username , workSpace)
        {
            Title = title;
            ProjectName = projectName;
            WorkSpace = workSpace;
            Date = date;
            Time = time;
            Description = description;
            WorkPlace = workPlace;
        }

        public string Title { get; set; }
        public string ProjectName { get; set; }
        public string WorkPlace { get; set; }
        public DateTime Date { get; set; }
        public int Time { get; set; }
        public string Description { get; set; }
    }
}
