﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommandApp.Commands.DutiesCommands
{
    public class DeleteDutyCommand : BaseCommand , IRequest
    {
        public DeleteDutyCommand(string username , string workSpace , string id ) : base(username , workSpace)
        {
            Id = id;
        }
        public string Id { get; set; }
    }
}
