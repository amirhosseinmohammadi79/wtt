﻿using Contract.Dtos.User;
using MediatR;

namespace CommandApp.Commands.AuthCommands
{
    public class LoginCommand : IRequest<LoginResultDto>
    {
        public LoginCommand(string username, string password)
        {
            Username = username;
            Password = password;
        }

        public string Username { get; set; }
        public string Password { get; set; }
    }
}