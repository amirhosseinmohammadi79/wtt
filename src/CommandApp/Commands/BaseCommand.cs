﻿namespace CommandApp.Commands;

public class BaseCommand
{
    public BaseCommand(string username, string workSpace)
    {
        Username = username;
        WorkSpace = workSpace;
    }

    public string Username { get; set; }
    public string WorkSpace { get; set; }
}