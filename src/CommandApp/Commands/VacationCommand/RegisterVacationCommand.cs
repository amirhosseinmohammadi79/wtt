﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommandApp.Commands.VacationCommand
{
    public class RegisterVacationCommand : BaseCommand , IRequest
    {
        public RegisterVacationCommand(string username , string workSpace , string description , DateTime date) : base(username , workSpace)
        {
            Description = description;
            Date = date;
        }
        public string Description { get; set; }
        public DateTime Date { get; set; }

    }
}
