﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommandApp.Commands.VacationCommand
{
    public class DeleteVacationCommand : BaseCommand , IRequest
    {
        public DeleteVacationCommand(string username , string workSpace , string id) : base(username , workSpace)
        {
            Id = id;
        }
        public string Id { get; set; }
    }
}
