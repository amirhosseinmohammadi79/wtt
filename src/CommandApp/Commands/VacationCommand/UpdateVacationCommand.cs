﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommandApp.Commands.VacationCommand
{
    public class UpdateVacationCommand : BaseCommand, IRequest
    {
        public UpdateVacationCommand(string username, string workSpace, string id, string description, DateTime date):base(username , workSpace)
        {
            Id = id;
            Description = description;
            Date = date;
        }
        public string Id { get; set; }
        public string Description { get; set; }
        public DateTime Date { get; set; }
    }
}
