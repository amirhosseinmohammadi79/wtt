﻿using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;

namespace Framework.Extensions;

public static class ServiceCollectionExtensions
{
    public static void AddCustomCorsSettings(this IServiceCollection services)
    {
        services.AddCors(options => { options.AddPolicy(name: "CORS",
            corsPolicyBuilder =>
            {
                corsPolicyBuilder
                    .WithOrigins("http://localhost:4200")
                    .AllowAnyHeader()
                    .AllowAnyMethod();
            }); });
    }

}