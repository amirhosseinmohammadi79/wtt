﻿using CommandApp.Commands.AuthCommands;
using Contract.Dtos.User;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace WebApi.Controllers.v1
{
    [ApiVersion("1.0")]
    [Route("api/v{version}/[controller]")]

    public class AuthController : BaseController
    {

        private readonly ISender _sender;

        public AuthController(IHttpContextAccessor httpContextAccessor, ISender sender) : base(httpContextAccessor)
        {
            _sender = sender;
        }

        [AllowAnonymous]
        [HttpPost("Login")]
        public async Task<IActionResult> Login([FromBody] LoginDto loginDto)
        {
            try
            {
                return Ok(
                    await _sender.Send(new LoginCommand(
                        loginDto.Username,
                        loginDto.Password
                    ))
                );
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
       
        }
    }
}