﻿using CommandApp.Commands.PresencesCommand;
using CommandApp.Commands.VacationCommand;
using Contract.Dtos.Presence;
using Contract.Dtos.Vacation;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using QueryApp.Queries;

namespace WebApi.Controllers.v1
{
    [ApiVersion("1.0")]
    public class VacationController : BaseController
    {
        private readonly ISender _sender;

        public VacationController(IHttpContextAccessor httpContextAccessor, ISender sender) : base(httpContextAccessor)
        {
            _sender = sender;
        }


        [HttpGet("GetAll")]
        public async Task<IActionResult> GetAllVacations([FromQuery] GetVacationDto getVacationDto)
        {
            return Ok(
                await _sender.Send(new GetVacationsQuery(
                    Username,
                    WorkSpace,
                    getVacationDto.FromDate ?? new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1),
                    getVacationDto.ToDate ?? new DateTime(DateTime.Now.Year, DateTime.Now.Month,
                        DateTime.DaysInMonth(DateTime.Now.Year, DateTime.Now.Month))
                ))
            );
        }

        [HttpPost("Register")]
        public async Task<IActionResult> RegisterVacation([FromBody] RegisterVacationDto registerVacationDto)
        {
            return Ok(
                await _sender.Send(new RegisterVacationCommand(
                    Username,
                    WorkSpace,
                    registerVacationDto.Description,
                    registerVacationDto.Date
                ))
            );
        }

        [HttpPut("Update")]
        public async Task<IActionResult> UpdateVacation([FromBody] UpdateVacationDto updateVacationDto)
        {
            return Ok(
                await _sender.Send(new UpdateVacationCommand(
                    Username,
                    WorkSpace,
                    updateVacationDto.Id,
                    updateVacationDto.Description,
                    updateVacationDto.Date
                ))
            );
        }


        [HttpDelete("Delete")]
        public async Task<IActionResult> DeleteVacation([FromQuery] DeleteVacationDto deleteVacationDto)
        {
            return Ok(
                await _sender.Send(new DeleteVacationCommand(
                    Username,
                    WorkSpace,
                    deleteVacationDto.Id
                ))
            );
        }
    }
}