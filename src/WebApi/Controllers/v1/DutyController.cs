﻿using CommandApp.Commands.DutiesCommands;
using CommandApp.Commands.Duty;
using Contract.Dtos.Duty;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using QueryApp.Queries;

namespace WebApi.Controllers.v1
{
    [ApiVersion("1.0")]

    public class DutyController : BaseController
    {
        private readonly ISender _sender;

        public DutyController(IHttpContextAccessor httpContextAccessor, ISender sender) : base(httpContextAccessor)
        {
            _sender = sender;
        }

        [HttpGet("GetAll")]
        public async Task<IActionResult> GetAllDuties([FromQuery] GetDutyDto dutyDto)
        {
            return Ok(
                await _sender.Send(new GetDutiesQuery(
                    Username,
                    WorkSpace,
                    dutyDto.Title,
                    dutyDto.ProjectName,
                    dutyDto.FromDate ?? new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1),
                    dutyDto.ToDate ?? new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.DaysInMonth(DateTime.Now.Year, DateTime.Now.Month))
                    ))
                );
        }

        [HttpPost("Register")]
        public async Task<IActionResult> RegisterDuty([FromBody] RegisterDutyDto dutyDto)
        {
            return Ok(
                await _sender.Send(new RegisterDutyCommand(
                    Username,
                    WorkSpace,
                    dutyDto.Title,
                    dutyDto.ProjectName,
                    dutyDto.WorkPlace,
                    dutyDto.Date,
                    dutyDto.Time,
                    dutyDto.Description
                    ))
                );
        }

        [HttpPut("Update")]
        public async Task<IActionResult> UpdateDuty([FromBody] UpdateDutyDto dutyDto)
        {
            return Ok(
                await _sender.Send(new UpdateDutyCommand(
                    Username,
                    WorkSpace,
                    dutyDto.Id,
                    dutyDto.Title,
                    dutyDto.ProjectName,
                    dutyDto.WorkPlace,
                    dutyDto.Date,
                    dutyDto.Time,
                    dutyDto.Description
                    ))
                );
        }

        [HttpDelete("Delete")]
        public async Task<IActionResult> DeleteDuty([FromQuery] DeleteDutyDto dutyDto)
        {
            return Ok(
                await _sender.Send(new DeleteDutyCommand(
                    Username,
                    WorkSpace,
                    dutyDto.Id
                    ))
                );
        }
    }
}
