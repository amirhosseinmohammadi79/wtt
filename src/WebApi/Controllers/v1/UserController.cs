﻿using CommandApp.Commands.Duty;
using CommandApp.Commands.UserCommand;
using Contract.Dtos.Duty;
using Contract.Dtos.User;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using QueryApp.Queries;

namespace WebApi.Controllers.v1
{
    [ApiVersion("1.0")]
    public class UserController : BaseController
    {
        private readonly ISender _sender;

        public UserController(IHttpContextAccessor httpContextAccessor, ISender sender) : base(httpContextAccessor)
        {
            _sender = sender;
        }


        [HttpGet("GetAll")]
        public async Task<IActionResult> GetAllUsers([FromQuery] GetUsersDto usersDto)
        {
            var roleClaim = User.Claims.FirstOrDefault(c => c.Type == "Role")?.Value;
            if (roleClaim != "Admin")
                return BadRequest("Role Exception");

            return Ok(
                await _sender.Send(new GetUsersQuery(
                    usersDto.Username,
                    usersDto.FirstName,
                    usersDto.LastName,
                    usersDto.NationalId
                ))
            );
        }

        [HttpPost("Register")]
        public async Task<IActionResult> RegisterUser([FromBody] RegisterUserDto userDto)
        {
            var roleClaim = User.Claims.FirstOrDefault(c => c.Type == "Role")?.Value;
            if (roleClaim != "Admin")
                return BadRequest("Role Exception");

            return Ok(
                await _sender.Send(new RegisterUserCommand(
                    userDto.Username,
                    userDto.FirstName,
                    userDto.LastName,
                    userDto.Password,
                    userDto.Role,
                    userDto.NationalId,
                    userDto.PhoneNumber,
                    userDto.Email,
                    userDto.WorkSpace
                ))
            );
        }


        [HttpPut("Update")]
        public async Task<IActionResult> UpdateUser([FromBody] UpdateUserDto updateUserDto)
        {
            var roleClaim = User.Claims.FirstOrDefault(c => c.Type == "Role")?.Value;
            if (roleClaim != "Admin")
                return BadRequest("Role Exception");

            return Ok(
                await _sender.Send(new UpdateUserCommand(
                    updateUserDto.Username,
                    updateUserDto.FirstName,
                    updateUserDto.LastName,
                    updateUserDto.Role,
                    updateUserDto.NationalId,
                    updateUserDto.PhoneNumber,
                    updateUserDto.Email,
                    updateUserDto.WorkSpace
                ))
            );
        }


        [HttpDelete("Delete")]
        public async Task<IActionResult> DeleteUser([FromQuery] DeleteUserDto deleteUserDto)
        {
            var roleClaim = User.Claims.FirstOrDefault(c => c.Type == "Role")?.Value;
            if (roleClaim != "Admin")
                return BadRequest("Role Exception");

            return Ok(
                await _sender.Send(new DeleteUserCommand(
                    deleteUserDto.Username,
                    deleteUserDto.WorkSpace
                ))
            );
        }
    }
}