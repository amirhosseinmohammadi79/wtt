﻿using Contract.Dtos.Dashboard;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using QueryApp.Queries;

namespace WebApi.Controllers.v1;


[ApiVersion("1.0")]

public class DashboardController : BaseController
{
    private readonly ISender _sender;

    public DashboardController(IHttpContextAccessor httpContextAccessor, ISender sender) : base(httpContextAccessor)
    {
        _sender = sender;
    }
    
    
    [HttpGet("GetAll")]
    public async Task<IActionResult> GetDashboardInfo([FromQuery] GetDashboardView dashboardView)
    {
        return Ok(
            await _sender.Send(new GetDashboardInfoQuery(
                Username,
                WorkSpace,
                dashboardView.FromDate ?? new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1),
                dashboardView.ToDate ?? new DateTime(DateTime.Now.Year, DateTime.Now.Month,
                    DateTime.DaysInMonth(DateTime.Now.Year, DateTime.Now.Month))
            ))
        );
    }
    
    
}