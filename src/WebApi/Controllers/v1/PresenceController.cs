﻿using CommandApp.Commands.DutiesCommands;
using CommandApp.Commands.Duty;
using CommandApp.Commands.PresencesCommand;
using Contract.Dtos.Duty;
using Contract.Dtos.Presence;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using QueryApp.Queries;

namespace WebApi.Controllers.v1
{
    [ApiVersion("1.0")]
    public class PresenceController : BaseController
    {
        private readonly ISender _sender;

        public PresenceController(IHttpContextAccessor httpContextAccessor, ISender sender) : base(httpContextAccessor)
        {
            _sender = sender;
        }

        [HttpGet("GetAll")]
        public async Task<IActionResult> GetAllPresence([FromQuery] GetPresenceDto getPresenceDto)
        {
            return Ok(
                await _sender.Send(new GetPresencesQuery(
                    Username,
                    WorkSpace,
                    getPresenceDto.Id,
                    getPresenceDto.FromDate ?? new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1),
                    getPresenceDto.ToDate ?? new DateTime(DateTime.Now.Year, DateTime.Now.Month,
                        DateTime.DaysInMonth(DateTime.Now.Year, DateTime.Now.Month))
                ))
            );
        }

        [HttpPost("Register")]
        public async Task<IActionResult> RegisterPresence([FromBody] RegisterPresenceDto presenceDto)
        {
            return Ok(
                await _sender.Send(new RegisterPresenceCommand(
                    Username,
                    WorkSpace,
                    presenceDto.Date,
                    presenceDto.StartTime,
                    presenceDto.EndTime
                ))
            );
        }

        [HttpPut("Update")]
        public async Task<IActionResult> UpdatePresence([FromBody] UpdatePresenceDto presenceDto)
        {
            return Ok(
                await _sender.Send(new UpdatePresenceCommand(
                    Username,
                    WorkSpace,
                    presenceDto.Id,
                    presenceDto.Date,
                    presenceDto.StartTime,
                    presenceDto.EndTime
                ))
            );
        }

        [HttpDelete("Delete")]
        public async Task<IActionResult> DeletePresence([FromQuery] DeletePresenceDto deletePresenceDto)
        {
            return Ok(
                await _sender.Send(new DeletePresenceCommand(
                    Username,
                    WorkSpace,
                    deletePresenceDto.Id
                ))
            );
        }
    }
}