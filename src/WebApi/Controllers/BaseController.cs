﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace WebApi.Controllers;

[ApiController]
 // [Authorize]
[Route("api/v{version}/[controller]")]
public class BaseController : ControllerBase
{
    protected string? Username { get; set; }
    protected string? WorkSpace { get; set; }

    private readonly IHttpContextAccessor _httpContextAccessor;

    public BaseController(IHttpContextAccessor httpContextAccessor)
    {
        _httpContextAccessor = httpContextAccessor;
        Username = _httpContextAccessor.HttpContext?.User.Claims.SingleOrDefault(x => x.Type == "Username")?.Value;
        WorkSpace = _httpContextAccessor.HttpContext?.User.Claims.SingleOrDefault(x => x.Type == "WorkSpace")?.Value;
    }
}