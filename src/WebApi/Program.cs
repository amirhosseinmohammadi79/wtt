using CommandApp.Interfaces;
using Core.Settings;
using Framework.Extensions;
using Framework.Extensions.Swagger;
using MediatR;
using QueryApp.Interfaces;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

builder.Services.AddControllers();
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwagger();
builder.Services.AddServices();
builder.Services.AddCustomCorsSettings();
builder.Services.InitSettings(builder.Configuration);
builder.Services.AddJwtAuthentication(builder.Configuration.GetSection(nameof(Settings)).Get<Settings>().JwtSettings);
builder.Services.AddMediatR(typeof(ICommandApp).Assembly);
builder.Services.AddMediatR(typeof(IQueryApp).Assembly);

var app = builder.Build();

if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
    app.UseCors("CORS");
}

app.UseAuthentication();
app.UseAuthorization();

app.MapControllers();

app.Run();