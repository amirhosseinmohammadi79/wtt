﻿namespace Entities.Models;

public class DashboardView
{
    public string TotalTime { get; set; }
    public string ExpectedAttendance { get; set; }
    public string OverTime { get; set; }
    public string Income { get; set; }
    public int DayCount { get; set; }
}