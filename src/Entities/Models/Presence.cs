﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entities.Models
{
    public class Presence
    {
        public string Id { get; set; }
        public DateTime Date { get; set; }
        public DateTime RegDateTime { get; set; }
        public string StartTime { get; set; }
        public string EndTime { get; set; }
        public string TotalPresenceTime { get; set; }
        public bool IsDeleted { get; set; }


    }
}
