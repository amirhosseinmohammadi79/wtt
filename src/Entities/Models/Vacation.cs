﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entities.Models
{
    public class Vacation
    {
        public string Id { get; set; }
        public string Description { get; set; }
        public bool IsDeleted { get; set; }
        public bool Accepted { get; set; }
        public DateTime Date { get; set; }
        public DateTime RegDateTime { get; set; }
    }
}
