﻿namespace Entities.Models;

public class UserResponse
{
    public string Username { get; set; }
    public string FirstName { get; set; }
    public string LastName { get; set; }
    public string Role { get; set; }
    public string WorkSpace { get; set; }
    public string NationalId { get; set; }
    public string PhoneNumber { get; set; }
    public string Email { get; set; }
    public bool IsDeleted { get; set; }
    public DateTime RegDateTime { get; set; }
}