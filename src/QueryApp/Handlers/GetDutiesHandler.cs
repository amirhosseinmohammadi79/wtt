﻿using Contract.Dtos.Duty;
using ElasticRepository.Repository;
using Entities.Models;
using MediatR;
using QueryApp.Queries;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QueryApp.Handlers
{
    public class GetDutiesHandler : IRequestHandler<GetDutiesQuery, List<Duty>>
    {
        private readonly DutyRepository _dutyRepository;
        public GetDutiesHandler(DutyRepository dutyRepository)
        {
            _dutyRepository = dutyRepository;
        }

        public async Task<List<Duty>> Handle(GetDutiesQuery request, CancellationToken cancellationToken)
        {
            var getDuty = new GetDutyDto
            {
                Title = request.Title,
                FromDate = request.FromDate,
                ToDate = request.ToDate,
                ProjectName = request.ProjectName,
                Username = request.Username,
                WorkSpace = request.WorkSpace,
            };

            return await _dutyRepository.GetDuties(getDuty);
        }
    }
}
