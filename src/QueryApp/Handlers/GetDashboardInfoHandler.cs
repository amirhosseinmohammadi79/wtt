﻿using System.Globalization;
using Contract.Dtos.Presence;
using ElasticRepository.Repository;
using Entities.Models;
using MediatR;
using QueryApp.Queries;

namespace QueryApp.Handlers;

public class GetDashboardInfoHandler : IRequestHandler<GetDashboardInfoQuery , DashboardView>
{
    private readonly PresenceRepository _presenceRepository;
    private readonly UserRepository _userRepository;

    public GetDashboardInfoHandler(PresenceRepository presenceRepository, UserRepository userRepository)
    {
        _presenceRepository = presenceRepository;
        _userRepository = userRepository;
    }

    public async Task<DashboardView> Handle(GetDashboardInfoQuery request, CancellationToken cancellationToken)
    {
        var getPresences = new GetPresenceDto
        {
            FromDate = request.FromDate,
            ToDate = request.ToDate,
            Username = request.Username,
            WorkSpace = request.WorkSpace
        };

        var user = await _userRepository.GetUser(request.Username , request.WorkSpace);
        var presences = await _presenceRepository.GetPresences(getPresences);
        var presenceDistinct = new List<Presence>();
        // Distinct

        foreach (var presence in presences)
        {
            var date = presence.Date.Date;
            if (!presenceDistinct.Any(x=> x.Date.Date == date))
            {
                presenceDistinct.Add(presence);
            }
        }
        
        

        // Sum of Presence
        TimeSpan totalTime = TimeSpan.Zero;
        foreach (var presence in presenceDistinct)
        {
            TimeSpan presenceTime;
            if (TimeSpan.TryParse(presence.TotalPresenceTime, out presenceTime))
            {
                totalTime = totalTime.Add(presenceTime);
            }
        }
        var totalTimeHour = totalTime.Days * 24 + totalTime.Hours;;
        var totalTimeMinuts =totalTime.Minutes ;
        var totalTimeFormat =totalTimeHour.ToString("00") + ":" + totalTimeMinuts.ToString("00"); ;

        
        

        // Presence Days
        var dayCount = presenceDistinct.Count();

        // expectedAttendance
        TimeSpan expectedTime;
        var expectedAttendance = TimeSpan.Zero;
        var expectedAttendanceTimeFormat = "";
        if (TimeSpan.TryParse("08:48:00", out expectedTime))
        {
            expectedAttendance = (expectedTime * presenceDistinct.Count());
            int expectedAttendanceHours = expectedAttendance.Days * 24 + expectedAttendance.Hours;
            int expectedAttendanceMinutes = expectedAttendance.Minutes;
            expectedAttendanceTimeFormat = expectedAttendanceHours.ToString("00") + ":" + expectedAttendanceMinutes.ToString("00");
        }

        // overtime
        var overTime = totalTime - expectedAttendance;
        var overTimeHour = overTime.Days * 24 + overTime.Hours;
        var overTimeMinuts = overTime.Minutes;
        var overTimeFormat =  overTimeHour.ToString("00") + ":" + overTimeMinuts.ToString("00");
        
        
        // Income
        int gradeFactor = 0;

        switch (user.Role)
        {
            case "Admin" :
                gradeFactor = 30000;
                break;
            
            case "Developer" :
                gradeFactor = 38000;
                break;
            
            case "Senior" :
                gradeFactor = 58000;
                break;
            
            case "SE" :
                gradeFactor = 78000;
                break;
            
        }
        
     
        var totalHours = (totalTime + overTime).TotalHours;
        double income = totalHours * gradeFactor;


        var dashboardView = new DashboardView()
        {
            DayCount = dayCount,
            ExpectedAttendance = expectedAttendanceTimeFormat,
            OverTime = overTimeFormat,
            TotalTime = totalTimeFormat,
            Income = income.ToString("N0", CultureInfo.InvariantCulture)
        };

        return dashboardView;
    }
}