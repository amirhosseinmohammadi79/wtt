﻿using Contract.Dtos.Duty;
using Contract.Dtos.Vacation;
using ElasticRepository.Repository;
using Entities.Models;
using MediatR;
using QueryApp.Queries;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QueryApp.Handlers
{
    public class GetVacationsHandler : IRequestHandler<GetVacationsQuery , List<Vacation>>
    {
        private readonly VacationRepository _vacationRepository;
        public GetVacationsHandler(VacationRepository vacationRepository)
        {
            _vacationRepository = vacationRepository;
        }

        public async Task<List<Vacation>> Handle(GetVacationsQuery request, CancellationToken cancellationToken)
        {
            var getVacation = new GetVacationDto
            {
                FromDate = request.FromDate,
                ToDate = request.ToDate,
                Username = request.Username,
                WorkSpace = request.WorkSpace
            };

            return await _vacationRepository.GetVacations(getVacation);
        }
    }
}
