﻿using Contract.Dtos.User;
using ElasticRepository.Repository;
using Entities.Models;
using MediatR;
using QueryApp.Queries;

namespace QueryApp.Handlers;

public class GetUsersHandler : IRequestHandler<GetUsersQuery, List<UserResponse>>
{
    private readonly UserRepository _userRepository;

    public GetUsersHandler(UserRepository userRepository)
    {
        _userRepository = userRepository;
    }

    public async Task<List<UserResponse>> Handle(GetUsersQuery request, CancellationToken cancellationToken)
    {
        var getUserDto = new GetUsersDto()
        {
            Username = request.Username,
            FirstName = request.FirstName,
            LastName = request.LastName,
            NationalId = request.NationalId
        };

        return await _userRepository.GetUsers(getUserDto);
    }
}