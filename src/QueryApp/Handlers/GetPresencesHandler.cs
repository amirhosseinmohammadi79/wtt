﻿using Contract.Dtos.Duty;
using Contract.Dtos.Presence;
using ElasticRepository.Repository;
using Entities.Models;
using MediatR;
using QueryApp.Queries;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QueryApp.Handlers
{
    public class GetPresencesHandler  : IRequestHandler<GetPresencesQuery , List<Presence>>
    {
        private readonly PresenceRepository _presenceRepository;
        public GetPresencesHandler(PresenceRepository presenceRepository)
        {
            _presenceRepository = presenceRepository;
        }

        public async Task<List<Presence>> Handle(GetPresencesQuery request, CancellationToken cancellationToken)
        {
            var getPresences = new GetPresenceDto
            {
                FromDate = request.FromDate,
                ToDate = request.ToDate,
                Id = request.Id,
                Username = request.Username,
                WorkSpace = request.WorkSpace
            };

            return await _presenceRepository.GetPresences(getPresences);
        }
    }
}
