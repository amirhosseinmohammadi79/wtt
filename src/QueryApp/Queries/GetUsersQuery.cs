﻿using Entities.Models;
using MediatR;

namespace QueryApp.Queries;

public class GetUsersQuery : IRequest<List<UserResponse>>
{
    public GetUsersQuery(string username, string firstName, string lastName, string nationalId)
    {
        Username = username;
        FirstName = firstName;
        LastName = lastName;
        NationalId = nationalId;
    }

    public string? Username { get; set; }
    public string? FirstName { get; set; }
    public string? LastName { get; set; }
    public string? NationalId { get; set; }
}