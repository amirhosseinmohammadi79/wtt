﻿using Entities.Models;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QueryApp.Queries
{
    public class GetDutiesQuery : BaseQuery , IRequest<List<Duty>>
    {
        public GetDutiesQuery(string username , string workSpace , string title , string projectName , DateTime fromDate , DateTime toDate)
            : base(username , workSpace)
        {
            Title = title;
            ProjectName = projectName;
            FromDate = fromDate;
            ToDate = toDate;
        }
        public string Title { get; set; }
        public string ProjectName { get; set; }
        public DateTime? FromDate { get; set; } = default;
        public DateTime? ToDate { get; set; } = default;
    }
}
