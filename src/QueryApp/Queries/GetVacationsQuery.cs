﻿using Entities.Models;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QueryApp.Queries
{
    public class GetVacationsQuery : BaseQuery, IRequest<List<Vacation>>
    {
        public GetVacationsQuery(string username, string workSpace, DateTime fromDate, DateTime toDate) : base(username, workSpace)
        {
            FromDate = fromDate;
            ToDate = toDate;
        }

        public DateTime? FromDate { get; set; }
        public DateTime? ToDate { get; set; }
    }
}
