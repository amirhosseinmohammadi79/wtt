﻿using Entities.Models;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QueryApp.Queries
{
    public class GetPresencesQuery : BaseQuery , IRequest<List<Presence>>
    {

        public GetPresencesQuery(string username , string workSpace,string id ,DateTime fromDate, DateTime toDate) : base(username , workSpace)
        {
            FromDate = fromDate;
            ToDate = toDate;
            Id = id;
        }

        public string Id { get; set; }
        public DateTime? FromDate { get; set; } = default;
        public DateTime? ToDate { get; set; } = default;
    }
}
