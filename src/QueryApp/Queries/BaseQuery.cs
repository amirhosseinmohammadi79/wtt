﻿namespace QueryApp.Queries;

public class BaseQuery
{
    public BaseQuery(string username, string workSpace)
    {
        Username = username;
        WorkSpace = workSpace;
    }

    public string Username { get; set; }
    public string WorkSpace { get; set; }
}