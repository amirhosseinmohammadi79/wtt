﻿using Entities.Models;
using MediatR;

namespace QueryApp.Queries;

public class GetDashboardInfoQuery : BaseQuery , IRequest<DashboardView>
{
    public GetDashboardInfoQuery(string username , string workSpace , DateTime fromDate , DateTime toDate)  :base(username , workSpace)
    {
        FromDate = fromDate;
        ToDate = toDate;
    }
    
    public DateTime FromDate { get; set; }
    public DateTime ToDate { get; set; }
}